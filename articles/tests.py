# -*- coding: utf-8 -*-
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test.client import Client
from django_dynamic_fixture import G
from models import Article


class ArticlesTest(TestCase):

    def setUp(self):
        super(ArticlesTest, self).setUp()
        self.user = G(User, username='test', is_active=True)
        self.user.set_password('test')
        self.user.save()

        self.client = Client()
        result_login = self.client.login(
            username='test',
            password='test')
        self.assertTrue(result_login)


class ArticlesListViewTest(ArticlesTest):

    def test_get(self):
        G(Article)
        G(Article)
        G(Article)

        response = self.client.get(reverse('article-list'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['article_list']), 3)


class ArticleCreateViewTest(ArticlesTest):

    def test_get(self):
        response = self.client.get(reverse('article-create'))

        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)

    def test_post(self):
        data = {
            'title': 'test',
            'text': 'test'
        }

        response = self.client.post(
            reverse('article-create'), data, follow=True)

        self.assertEqual(response.status_code, 200)

        article = Article.objects.all()[0]
        self.assertEqual(article.title, data['title'])
        self.assertEqual(article.text, data['text'])
        self.assertEqual(article.user, self.user)


class ArticleDetailViewTest(ArticlesTest):

    def test_get(self):
        article = G(Article)

        response = self.client.get(
            reverse('article-detail', args=[article.id]))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['article'], article)


class ArticleUpdateViewTest(ArticlesTest):

    def test_permission(self):
        article = G(Article, user=G(User))

        response = self.client.get(
            reverse('article-update', args=[article.id]))

        self.assertEqual(response.status_code, 403)

    def test_get(self):
        article = G(Article, user=self.user)

        response = self.client.get(
            reverse('article-update', args=[article.id]))

        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)

    def test_post(self):
        article = G(Article, user=self.user)
        data = {
            'title': 'test1',
            'text': 'test1'
        }

        response = self.client.post(
            reverse('article-update', args=[article.id]), data, follow=True)

        self.assertEqual(response.status_code, 200)

        article = Article.objects.get(id=article.id)
        self.assertEqual(article.title, data['title'])
        self.assertEqual(article.text, data['text'])


class ArticlesDeleteViewTest(ArticlesTest):

    def test_permission(self):
        article = G(Article, user=G(User))

        response = self.client.get(
            reverse('article-delete', args=[article.id]))

        self.assertEqual(response.status_code, 403)

    def test_delete(self):
        article = G(Article, user=self.user)

        response = self.client.get(
            reverse('article-delete', args=[article.id]))

        self.assertEqual(response.status_code, 302)
        self.assertEqual(Article.objects.filter(id=article.id).count(), 0)
