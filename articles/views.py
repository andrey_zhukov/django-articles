# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from models import Article
from forms import ArticleForm


class LoginRequiredMixin(object):
    """
    View mixin which verifies that the user has authenticated
    """

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class CheckObjectUserMixin(SingleObjectMixin):
    """
    View mixin which check user of object
    """

    def get_object(self, queryset=None):
        obj = super(CheckObjectUserMixin, self).get_object(queryset)
        if obj.user_id != self.request.user.id:
            raise PermissionDenied
        return obj


class ArticleListView(ListView):
    model = Article


class ArticleCreateView(LoginRequiredMixin, CreateView):
    model = Article
    form_class = ArticleForm

    def form_valid(self, form):
        article = form.save(commit=False)
        article.user = self.request.user
        article.save()
        messages.success(self.request, u'Article created.')
        return super(ArticleCreateView, self).form_valid(form)


class ArticleDetailView(DetailView):
    model = Article


class ArticleUpdateView(LoginRequiredMixin, CheckObjectUserMixin, UpdateView):
    model = Article
    form_class = ArticleForm

    def form_valid(self, form):
        messages.success(self.request, u'Article updated.')
        return super(ArticleUpdateView, self).form_valid(form)

    def dispatch(self, request, *args, **kwargs):
        return super(ArticleUpdateView, self).dispatch(
            request, *args, **kwargs)


class ArticleDeleteView(LoginRequiredMixin, CheckObjectUserMixin, DeleteView):
    model = Article
    success_url = reverse_lazy('article-list')

    def get(self, *args, **kwargs):
        """
        Skip confirmation page
        """
        return self.delete(self.request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, u'Article removed.')
        return super(ArticleDeleteView, self).delete(*args, **kwargs)
