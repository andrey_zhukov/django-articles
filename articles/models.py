# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Article(models.Model):
    user = models.ForeignKey(User, related_name='articles')
    title = models.CharField(max_length=255)
    text = models.TextField()

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('article-detail', [self.pk])
